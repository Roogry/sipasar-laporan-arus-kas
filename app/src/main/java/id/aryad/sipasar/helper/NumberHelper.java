package id.aryad.sipasar.helper;

import java.text.NumberFormat;
import java.util.Locale;

public class NumberHelper {
    private static final Locale localeID = new Locale("in", "ID");

    public static String currency(int nominal) {
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);

        return formatRupiah.format(nominal);
    }
}
