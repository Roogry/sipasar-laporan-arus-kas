package id.aryad.sipasar.helper;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPref {
    private final SharedPreferences sharedPreferences;
    private final SharedPreferences.Editor sharedEditor;

    public SharedPref(Context context) {
        sharedPreferences = context.getSharedPreferences(KeyValue.SP_SESSION, Context.MODE_PRIVATE);
        sharedEditor = sharedPreferences.edit();
        sharedEditor.apply();
    }

    public void setBoolean(String key, Boolean value) {
        sharedEditor.putBoolean(key, value);
        sharedEditor.apply();
    }

    public Boolean getBoolean(String key) {
        return sharedPreferences.getBoolean(key, false);
    }

    public void setInt(String key, int value) {
        sharedEditor.putInt(key, value);
        sharedEditor.apply();
    }

    public int getInt(String key) {
        return sharedPreferences.getInt(key, 0);
    }

    public void setString(String key, String value) {
        sharedEditor.putString(key, value);
        sharedEditor.apply();
    }

    public String getString(String key) {
        return sharedPreferences.getString(key, "");
    }

    public void setSessionLogin(int idAdmin, String namaPegawai, String foto, String role) {
        setBoolean(KeyValue.SP_LOGGED_IN, true);
        setInt(KeyValue.SP_ADMIN_ID, idAdmin);
        setString(KeyValue.SP_NAME, namaPegawai);
        setString(KeyValue.SP_PHOTO, foto);
        setString(KeyValue.SP_ROLE, role);
    }

    public Boolean isLoggined() {
        return this.getBoolean(KeyValue.SP_LOGGED_IN);
    }

    public void clearSession() {
        sharedEditor.clear();
        sharedEditor.apply();
    }
}
