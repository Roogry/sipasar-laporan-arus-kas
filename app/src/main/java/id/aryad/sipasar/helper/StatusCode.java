package id.aryad.sipasar.helper;

public class StatusCode {
    // message code
    public static final int SUCCESS = 200;
    public static final int BAD_REQUEST = 400;
    public static final int FORBIDDEN = 403;
    public static final int NOT_FOUND = 404;
}
