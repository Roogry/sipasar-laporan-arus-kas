package id.aryad.sipasar.helper;

public class KeyValue {
    // shared pref
    public static final String SP_SESSION = "session";
    public static final String SP_LOGGED_IN = "is_logged_in";
    public static final String SP_ADMIN_ID = "id_admin";
    public static final String SP_NAME = "nama_pegawai";
    public static final String SP_PHOTO = "foto";
    public static final String SP_ROLE = "role";

    // extras intent
    public static final String EXT_PERIODE_ID = "id_periode_laporan";
}

