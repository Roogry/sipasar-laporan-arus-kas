package id.aryad.sipasar.helper;


import org.threeten.bp.LocalDateTime;
import org.threeten.bp.format.DateTimeFormatter;

public final class DateTimeHelper {
    private static final DateTimeFormatter parseDefault = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private static final DateTimeFormatter formatDate = DateTimeFormatter.ofPattern("dd MMM yyyy");

    public static LocalDateTime stringToDateTime(String dateString) {
        return LocalDateTime.parse(dateString, parseDefault);
    }

    public static String formatDate(String dateString) {
        LocalDateTime dateTime = LocalDateTime.parse(dateString, parseDefault);
        return dateTime.format(formatDate);
    }

    public static boolean isWithinRange(String checkDateString, String startDateString, String endDateString) {
        LocalDateTime checkDate, startDate, endDate;

        checkDate = stringToDateTime(checkDateString);
        startDate = stringToDateTime(startDateString);
        endDate = stringToDateTime(endDateString);

        return checkDate.isAfter(startDate) && checkDate.isBefore(endDate);
    }
}
