package id.aryad.sipasar.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import id.aryad.sipasar.R;
import id.aryad.sipasar.data.KategoriIuranData;
import id.aryad.sipasar.data.LapakData;
import id.aryad.sipasar.helper.DateTimeHelper;
import id.aryad.sipasar.helper.NumberHelper;
import id.aryad.sipasar.model.KategoriIuran;
import id.aryad.sipasar.model.Lapak;
import id.aryad.sipasar.model.PembayaranIuran;

public class IuranAdapter extends RecyclerView.Adapter<IuranAdapter.ViewHolder> {
    private final ArrayList<PembayaranIuran> listArusKas;

    public IuranAdapter(ArrayList<PembayaranIuran> listArusKas) {
        this.listArusKas = listArusKas;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_arus_kas, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindView(listArusKas.get(position));
    }

    @Override
    public int getItemCount() {
        return listArusKas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtTitle, txtDate, txtCategory, txtNominal;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtTitle = itemView.findViewById(R.id.txtTitle);
            txtDate = itemView.findViewById(R.id.txtDate);
            txtCategory = itemView.findViewById(R.id.txtCategory);
            txtNominal = itemView.findViewById(R.id.txtNominal);
        }

        public void bindView(PembayaranIuran arusKas) {
            KategoriIuran kategoriIuran = KategoriIuranData.getKategoriIuranById(arusKas.getId_kategori_iuran());
            Lapak lapak = LapakData.getLapakById(arusKas.getId_lapak());

            String nominal = NumberHelper.currency(arusKas.getNilai());
            String deskripsi = lapak.getNama_lapak() + " membayar iuran " + kategoriIuran.getNama_kategori_iuran();

            txtTitle.setText(deskripsi);
            txtDate.setText(DateTimeHelper.formatDate(arusKas.getTanggal_bayar()));
            txtCategory.setText("uang masuk");
            txtNominal.setText(nominal);
        }
    }
}
