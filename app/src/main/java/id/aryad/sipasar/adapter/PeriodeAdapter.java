package id.aryad.sipasar.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import id.aryad.sipasar.R;
import id.aryad.sipasar.helper.DateTimeHelper;
import id.aryad.sipasar.helper.KeyValue;
import id.aryad.sipasar.model.PeriodeLaporan;
import id.aryad.sipasar.ui.DetailArusKas.DetailArusKas;

public class PeriodeAdapter extends RecyclerView.Adapter<PeriodeAdapter.ViewHolder> {
    private final Context context;
    private final ArrayList<PeriodeLaporan> listPeriode;

    public PeriodeAdapter(Context context, ArrayList<PeriodeLaporan> listPeriode) {
        this.context = context;
        this.listPeriode = listPeriode;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_periode, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindView(context, listPeriode.get(position));
    }

    @Override
    public int getItemCount() {
        return listPeriode.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvDateRange;
        RelativeLayout rlPeriode;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvDateRange = itemView.findViewById(R.id.tvDateRange);
            rlPeriode = itemView.findViewById(R.id.rlPeriode);
        }

        public void bindView(Context context, PeriodeLaporan periode) {
            String dateRange = DateTimeHelper.formatDate(periode.getTanggal_awal_periode()) + " - " + DateTimeHelper.formatDate(periode.getTanggal_akhir_periode());

            tvTitle.setText(periode.getNama_periode());
            tvDateRange.setText(dateRange);

            rlPeriode.setOnClickListener(v -> {
                Intent toDetailArusKas = new Intent(context, DetailArusKas.class);
                toDetailArusKas.putExtra(KeyValue.EXT_PERIODE_ID, periode.getId_periode_laporan());
                context.startActivity(toDetailArusKas);
            });
        }
    }
}
