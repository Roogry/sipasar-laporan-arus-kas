package id.aryad.sipasar.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import id.aryad.sipasar.R;
import id.aryad.sipasar.data.PegawaiData;
import id.aryad.sipasar.helper.DateTimeHelper;
import id.aryad.sipasar.helper.NumberHelper;
import id.aryad.sipasar.model.Pegawai;
import id.aryad.sipasar.model.PembayaranGaji;

public class GajiAdapter extends RecyclerView.Adapter<GajiAdapter.ViewHolder> {
    private final Context context;
    private final ArrayList<PembayaranGaji> listGaji;

    public GajiAdapter(Context context, ArrayList<PembayaranGaji> listGaji) {
        this.context = context;
        this.listGaji = listGaji;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_arus_kas, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindView(listGaji.get(position));
    }

    @Override
    public int getItemCount() {
        return listGaji.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtTitle, txtDate, txtCategory, txtNominal;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtTitle = itemView.findViewById(R.id.txtTitle);
            txtDate = itemView.findViewById(R.id.txtDate);
            txtCategory = itemView.findViewById(R.id.txtCategory);
            txtNominal = itemView.findViewById(R.id.txtNominal);
        }

        public void bindView(PembayaranGaji gaji) {
            Pegawai pegawai = PegawaiData.getPegawaiById(gaji.getId_pegawai());

            String nominal = "-" + NumberHelper.currency(gaji.getNilai_gaji());
            String deskripsi = "Pembayaran gaji " + pegawai.getNama_pegawai();

            txtTitle.setText(deskripsi);
            txtDate.setText(DateTimeHelper.formatDate(gaji.getTanggal_bayar()));
            txtCategory.setText("uang keluar");
            txtNominal.setText(nominal);

            txtNominal.setTextColor(context.getResources().getColor(R.color.red_orange));
        }
    }
}
