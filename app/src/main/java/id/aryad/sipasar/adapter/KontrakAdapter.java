package id.aryad.sipasar.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import id.aryad.sipasar.R;
import id.aryad.sipasar.data.LapakData;
import id.aryad.sipasar.helper.DateTimeHelper;
import id.aryad.sipasar.helper.NumberHelper;
import id.aryad.sipasar.model.Lapak;
import id.aryad.sipasar.model.PembayaranKontrak;

public class KontrakAdapter extends RecyclerView.Adapter<KontrakAdapter.ViewHolder> {
    private final ArrayList<PembayaranKontrak> listKontrak;

    public KontrakAdapter(ArrayList<PembayaranKontrak> listKontrak) {
        this.listKontrak = listKontrak;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_arus_kas, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindView(listKontrak.get(position));
    }

    @Override
    public int getItemCount() {
        return listKontrak.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtTitle, txtDate, txtCategory, txtNominal;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtTitle = itemView.findViewById(R.id.txtTitle);
            txtDate = itemView.findViewById(R.id.txtDate);
            txtCategory = itemView.findViewById(R.id.txtCategory);
            txtNominal = itemView.findViewById(R.id.txtNominal);
        }

        public void bindView(PembayaranKontrak kontrak) {
            Lapak lapak = LapakData.getLapakById(kontrak.getId_lapak());

            String nominal = NumberHelper.currency(kontrak.getNilai());
            String deskripsi = lapak.getNama_lapak() + " membayar kontrak";

            txtTitle.setText(deskripsi);
            txtDate.setText(DateTimeHelper.formatDate(kontrak.getTanggal_bayar()));
            txtCategory.setText("uang masuk");
            txtNominal.setText(nominal);
        }
    }
}
