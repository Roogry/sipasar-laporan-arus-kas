package id.aryad.sipasar.model;

public class PeriodeLaporan {
    private int id_periode_laporan;
    private String nama_periode;
    private String tanggal_awal_periode;
    private String tanggal_akhir_periode;
    private int id_laporan_sebelumnya;
    private int uang_kas;

    public PeriodeLaporan(int id_periode_laporan, String nama_periode, String tanggal_awal_periode, String tanggal_akhir_periode, int id_laporan_sebelumnya, int uang_kas) {
        this.id_periode_laporan = id_periode_laporan;
        this.nama_periode = nama_periode;
        this.tanggal_awal_periode = tanggal_awal_periode;
        this.tanggal_akhir_periode = tanggal_akhir_periode;
        this.id_laporan_sebelumnya = id_laporan_sebelumnya;
        this.uang_kas = uang_kas;
    }

    public void increaseKas(int nominal) {
        this.uang_kas += nominal;
    }

    public void decreaseKas(int nominal) {
        this.uang_kas -= nominal;
    }

    public int getId_periode_laporan() {
        return id_periode_laporan;
    }

    public void setId_periode_laporan(int id_periode_laporan) {
        this.id_periode_laporan = id_periode_laporan;
    }

    public String getNama_periode() {
        return nama_periode;
    }

    public void setNama_periode(String nama_periode) {
        this.nama_periode = nama_periode;
    }

    public String getTanggal_awal_periode() {
        return tanggal_awal_periode;
    }

    public void setTanggal_awal_periode(String tanggal_awal_periode) {
        this.tanggal_awal_periode = tanggal_awal_periode;
    }

    public String getTanggal_akhir_periode() {
        return tanggal_akhir_periode;
    }

    public void setTanggal_akhir_periode(String tanggal_akhir_periode) {
        this.tanggal_akhir_periode = tanggal_akhir_periode;
    }

    public int getId_laporan_sebelumnya() {
        return id_laporan_sebelumnya;
    }

    public void setId_laporan_sebelumnya(int id_laporan_sebelumnya) {
        this.id_laporan_sebelumnya = id_laporan_sebelumnya;
    }

    public int getUang_kas() {
        return uang_kas;
    }

    public void setUang_kas(int uang_kas) {
        this.uang_kas = uang_kas;
    }

    @Override
    public String toString() {
        return nama_periode;
    }
}
