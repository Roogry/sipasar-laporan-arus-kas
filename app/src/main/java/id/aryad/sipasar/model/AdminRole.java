package id.aryad.sipasar.model;

public class AdminRole {
    public static final String ADMIN = "ADMIN";
    public static final String MANAGER = "MANAGER";
    public static final String PEGAWAI = "PEGAWAI";
}
