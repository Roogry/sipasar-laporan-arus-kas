package id.aryad.sipasar.model;

public class KategoriIuran {
    private final int id_kategori_iuran;
    private final String nama_kategori_iuran;
    private final int nilai;
    private final int status;

    public KategoriIuran(int id_kategori_iuran, String nama_kategori_iuran, int nilai, int status) {
        this.id_kategori_iuran = id_kategori_iuran;
        this.nama_kategori_iuran = nama_kategori_iuran;
        this.nilai = nilai;
        this.status = status;
    }

    public int getId_kategori_iuran() {
        return id_kategori_iuran;
    }

    public String getNama_kategori_iuran() {
        return nama_kategori_iuran;
    }

    public int getNilai() {
        return nilai;
    }

    public int getStatus() {
        return status;
    }
}
