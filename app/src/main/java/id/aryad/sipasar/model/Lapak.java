package id.aryad.sipasar.model;

public class Lapak {
    private final int id_lapak;
    private final int id_kategori_lapak;
    private final String nama_lapak;
    private final String nama_pemilik;
    private final String alamat_pemilik;
    private final String foto_pemilik;
    private final String posisi_lapak;
    private final int status;
    private final String tanggal_pendaftaran;
    private final String tanggal_akhir_kontrak;
    private final int id_admin;

    public Lapak(int id_lapak, int id_kategori_lapak, String nama_lapak, String nama_pemilik, String alamat_pemilik, String foto_pemilik, String posisi_lapak, int status, String tanggal_pendaftaran, String tanggal_akhir_kontrak, int id_admin) {
        this.id_lapak = id_lapak;
        this.id_kategori_lapak = id_kategori_lapak;
        this.nama_lapak = nama_lapak;
        this.nama_pemilik = nama_pemilik;
        this.alamat_pemilik = alamat_pemilik;
        this.foto_pemilik = foto_pemilik;
        this.posisi_lapak = posisi_lapak;
        this.status = status;
        this.tanggal_pendaftaran = tanggal_pendaftaran;
        this.tanggal_akhir_kontrak = tanggal_akhir_kontrak;
        this.id_admin = id_admin;
    }

    public String filterLapak(int status) {
        if (status == 1) {
            return "Aktif";
        } else {
            return "Non-Aktif";
        }

    }

    public int getId_lapak() {
        return id_lapak;
    }

    public int getId_kategori_lapak() {
        return id_kategori_lapak;
    }

    public String getNama_lapak() {
        return nama_lapak;
    }

    public String getNama_pemilik() {
        return nama_pemilik;
    }

    public String getAlamat_pemilik() {
        return alamat_pemilik;
    }

    public String getFoto_pemilik() {
        return foto_pemilik;
    }

    public String getPosisi_lapak() {
        return posisi_lapak;
    }

    public int getStatus() {
        return status;
    }

    public String getTanggal_pendaftaran() {
        return tanggal_pendaftaran;
    }

    public String getTanggal_akhir_kontrak() {
        return tanggal_akhir_kontrak;
    }

    public int getId_admin() {
        return id_admin;
    }
}
