package id.aryad.sipasar.model;

public class PembayaranGaji {
    private int id_pembayaran_gaji;
    private int id_pegawai;
    private int nilai_gaji;
    private String tanggal_bayar;

    public PembayaranGaji(int id_pembayaran_gaji, int id_pegawai, int nilai_gaji, String tanggal_bayar) {
        this.id_pembayaran_gaji = id_pembayaran_gaji;
        this.id_pegawai = id_pegawai;
        this.nilai_gaji = nilai_gaji;
        this.tanggal_bayar = tanggal_bayar;
    }

    public int getId_pembayaran_gaji() {
        return id_pembayaran_gaji;
    }

    public void setId_pembayaran_gaji(int id_pembayaran_gaji) {
        this.id_pembayaran_gaji = id_pembayaran_gaji;
    }

    public int getId_pegawai() {
        return id_pegawai;
    }

    public void setId_pegawai(int id_pegawai) {
        this.id_pegawai = id_pegawai;
    }

    public int getNilai_gaji() {
        return nilai_gaji;
    }

    public void setNilai_gaji(int nilai_gaji) {
        this.nilai_gaji = nilai_gaji;
    }

    public String getTanggal_bayar() {
        return tanggal_bayar;
    }

    public void setTanggal_bayar(String tanggal_bayar) {
        this.tanggal_bayar = tanggal_bayar;
    }
}
