package id.aryad.sipasar.data;

import java.util.ArrayList;
import java.util.List;

import id.aryad.sipasar.model.PeriodeLaporan;

public class PeriodeLaporanData {
    public static ArrayList<PeriodeLaporan> listPeriodeLaporan = new ArrayList<>(List.of(
            new PeriodeLaporan(4, "Kuartal IV 2020", "2020-10-01 00:00:01", "2020-12-31 23:59:59", 3, 0),
            new PeriodeLaporan(3, "Kuartal III 2020", "2020-07-01 00:00:01", "2020-09-30 23:59:59", 2, 0),
            new PeriodeLaporan(2, "Kuartal II 2020", "2020-04-01 00:00:01", "2020-06-30 23:59:59", 1, 0),
            new PeriodeLaporan(1, "Kuartal I 2020", "2020-01-01 00:00:01", "2020-03-31 23:59:59", 0, 0)
    ));

    public static int getPeriodePositionById(int idPeriode) {
        for (int i = 0; i < listPeriodeLaporan.size(); i++) {
            if (listPeriodeLaporan.get(i).getId_periode_laporan() == idPeriode) {
                return i;
            }
        }
        return 0;
    }

    public static PeriodeLaporan getPeriodeById(int idPeriode) {
        for (PeriodeLaporan periode : listPeriodeLaporan) {
            if (periode.getId_periode_laporan() == idPeriode) {
                return periode;
            }
        }
        return null;
    }
}
