package id.aryad.sipasar.data;

import java.util.ArrayList;
import java.util.List;

import id.aryad.sipasar.helper.DateTimeHelper;
import id.aryad.sipasar.model.PembayaranGaji;

public class PembayaranGajiData {
    public static ArrayList<PembayaranGaji> listPembayaranGaji = new ArrayList<>(List.of(
            new PembayaranGaji(1, 4, 1500000, "2020-06-06 07:26:38"),
            new PembayaranGaji(2, 4, 1700000, "2020-07-07 07:26:38"),
            new PembayaranGaji(3, 1, 2000000, "2020-10-10 07:26:38"),
            new PembayaranGaji(4, 2, 2000000, "2020-12-12 07:26:38")
    ));

    public static ArrayList<PembayaranGaji> getByPeriode(ArrayList<PembayaranGaji> pembayaranGajiList, String startDate, String endDate) {
        ArrayList<PembayaranGaji> pembayaranGajiPeriodeList = new ArrayList<>();
        for (PembayaranGaji pembayaranGaji : pembayaranGajiList) {
            if (DateTimeHelper.isWithinRange(pembayaranGaji.getTanggal_bayar(), startDate, endDate)) {
                pembayaranGajiPeriodeList.add(pembayaranGaji);
            }
        }

        return pembayaranGajiPeriodeList;
    }
}
