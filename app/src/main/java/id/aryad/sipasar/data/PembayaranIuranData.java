package id.aryad.sipasar.data;

import java.util.ArrayList;
import java.util.List;

import id.aryad.sipasar.helper.DateTimeHelper;
import id.aryad.sipasar.model.PembayaranIuran;

public class PembayaranIuranData {
    public static ArrayList<PembayaranIuran> listPembayaranIuran = new ArrayList<>(List.of(
            new PembayaranIuran(1, 1, "2020-01-01 07:26:38", "2020-01-01 07:26:38", 1, 1, 50000, 1),
            new PembayaranIuran(2, 3, "2020-01-01 07:26:38", "2020-01-01 07:26:38", 1, 1, 50000, 1),
            new PembayaranIuran(3, 2, "2020-02-04 07:26:38", "2020-04-04 07:26:38", 1, 2, 100000, 2),
            new PembayaranIuran(4, 3, "2020-03-05 07:26:38", "2020-05-05 07:26:38", 1, 3, 45000, 1),
            new PembayaranIuran(5, 2, "2020-04-04 07:26:38", "2020-04-04 07:26:38", 1, 2, 100000, 2),
            new PembayaranIuran(6, 1, "2020-04-09 07:26:38", "2020-09-09 07:26:38", 1, 1, 80000, 1),
            new PembayaranIuran(7, 1, "2020-05-01 07:26:38", "2020-01-01 07:26:38", 1, 1, 50000, 1),
            new PembayaranIuran(8, 2, "2020-06-04 07:26:38", "2020-04-04 07:26:38", 1, 2, 100000, 2),
            new PembayaranIuran(9, 3, "2020-07-05 07:26:38", "2020-05-05 07:26:38", 1, 3, 45000, 1),
            new PembayaranIuran(10, 3, "2020-08-09 07:26:38", "2020-09-09 07:26:38", 1, 1, 80000, 1),
            new PembayaranIuran(11, 1, "2020-09-01 07:26:38", "2020-01-01 07:26:38", 1, 1, 50000, 1),
            new PembayaranIuran(12, 2, "2020-10-04 07:26:38", "2020-04-04 07:26:38", 1, 2, 100000, 2),
            new PembayaranIuran(13, 1, "2020-10-09 07:26:38", "2020-09-09 07:26:38", 1, 1, 80000, 1),
            new PembayaranIuran(14, 3, "2020-11-05 07:26:38", "2020-05-05 07:26:38", 1, 3, 45000, 1),
            new PembayaranIuran(15, 2, "2020-12-09 07:26:38", "2020-09-09 07:26:38", 1, 1, 80000, 1),
            new PembayaranIuran(16, 3, "2020-07-05 07:26:38", "2020-05-05 07:26:38", 1, 3, 45000, 1)
    ));

    public static ArrayList<PembayaranIuran> getByPeriode(ArrayList<PembayaranIuran> pembayaranIuranList, String startDate, String endDate) {
        ArrayList<PembayaranIuran> pembayaranIuranPeriodeList = new ArrayList<>();
        for (PembayaranIuran pembayaranIuran : pembayaranIuranList) {
            if (DateTimeHelper.isWithinRange(pembayaranIuran.getTanggal_bayar(), startDate, endDate)) {
                pembayaranIuranPeriodeList.add(pembayaranIuran);
            }
        }

        return pembayaranIuranPeriodeList;
    }
}
