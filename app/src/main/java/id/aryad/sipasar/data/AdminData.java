package id.aryad.sipasar.data;

import java.util.ArrayList;
import java.util.List;

import id.aryad.sipasar.model.Admin;
import id.aryad.sipasar.model.AdminRole;

public class AdminData {
    public static ArrayList<Admin> listAdmin = new ArrayList<>(List.of(
            new Admin(1, 1, "jodie", "pass1234", AdminRole.MANAGER, 1),
            new Admin(2, 2, "sanchia", "123", AdminRole.ADMIN, 1),
            new Admin(3, 3, "mantra", "123", AdminRole.MANAGER, 0),
            new Admin(4, 4, "aryapegawai", "pass123", AdminRole.PEGAWAI, 1),
            new Admin(5, 4, "aryamanager", "pass123", AdminRole.MANAGER, 1),
            new Admin(6, 4, "aryaadmin", "pass123", AdminRole.ADMIN, 1)
    ));
}
