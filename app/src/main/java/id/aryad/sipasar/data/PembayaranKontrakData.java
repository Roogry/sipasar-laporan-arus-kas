package id.aryad.sipasar.data;

import java.util.ArrayList;
import java.util.List;

import id.aryad.sipasar.helper.DateTimeHelper;
import id.aryad.sipasar.model.PembayaranKontrak;

public class PembayaranKontrakData {
    public static ArrayList<PembayaranKontrak> listPembayaranKontrak = new ArrayList<>(List.of(
            new PembayaranKontrak(1, 1, "2020-02-02 07:26:38", "2020-02-02 07:26:38", "2020-03-12 07:26:38", 1500000, 1, 0, "0000-00-00"),
            new PembayaranKontrak(2, 3, "2020-05-08 07:26:38", "2020-05-08 07:26:38", "2020-06-12 07:26:38", 500000, 1, 0, "0000-00-00"),
            new PembayaranKontrak(3, 2, "2020-06-11 07:26:38", "2020-06-11 07:26:38", "2020-07-12 07:26:38", 1000000, 1, 0, "0000-00-00"),
            new PembayaranKontrak(4, 3, "2020-07-08 07:26:38", "2020-07-08 07:26:38", "2020-08-12 07:26:38", 500000, 1, 0, "0000-00-00"),
            new PembayaranKontrak(5, 2, "2020-08-11 07:26:38", "2020-08-11 07:26:38", "2020-09-12 07:26:38", 1000000, 1, 0, "0000-00-00"),
            new PembayaranKontrak(6, 3, "2020-10-08 07:26:38", "2020-10-08 07:26:38", "2020-11-12 07:26:38", 500000, 1, 0, "0000-00-00"),
            new PembayaranKontrak(7, 2, "2020-11-08 07:26:38", "2020-11-08 07:26:38", "2020-12-12 07:26:38", 500000, 1, 0, "0000-00-00"),
            new PembayaranKontrak(8, 1, "2020-12-08 07:26:38", "2020-12-08 07:26:38", "2021-01-12 07:26:38", 500000, 1, 0, "0000-00-00")
    ));

    public static ArrayList<PembayaranKontrak> getByPeriode(ArrayList<PembayaranKontrak> pembayaranKontrakList, String startDate, String endDate) {
        ArrayList<PembayaranKontrak> pembayaranKontrakPeriodeList = new ArrayList<>();
        for (PembayaranKontrak pembayaranKontrak : pembayaranKontrakList) {
            if (DateTimeHelper.isWithinRange(pembayaranKontrak.getTanggal_bayar(), startDate, endDate)) {
                pembayaranKontrakPeriodeList.add(pembayaranKontrak);
            }
        }

        return pembayaranKontrakPeriodeList;
    }
}
