package id.aryad.sipasar.data;

import java.util.ArrayList;
import java.util.List;

import id.aryad.sipasar.model.KategoriLapak;

public class KategoriLapakData {
    public static ArrayList<KategoriLapak> listKategoriLapak = new ArrayList<>(List.of(
            new KategoriLapak(1, "elektronik"),
            new KategoriLapak(2, "makanan"),
            new KategoriLapak(3, "buku")
    ));

    public static KategoriLapak getKategoriLapakById(int kategotiId) {
        for (KategoriLapak kategori : listKategoriLapak) {
            if (kategori.getId_kategori_lapak() == kategotiId) {
                return kategori;
            }
        }
        return null;
    }
}
