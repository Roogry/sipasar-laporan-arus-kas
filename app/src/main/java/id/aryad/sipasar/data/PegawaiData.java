package id.aryad.sipasar.data;

import java.util.ArrayList;
import java.util.List;

import id.aryad.sipasar.model.Pegawai;

public class PegawaiData {
    public static ArrayList<Pegawai> listPegawai = new ArrayList<>(List.of(
            new Pegawai(1, "Sanchia Jodie", "Denpasar", "https://tinyfac.es/data/avatars/D6128025-46D1-4AFF-84E3-DDF75CB157ED-500w.jpeg"),
            new Pegawai(2, "Sanchia Jodie Mantra", "Tabanan", "https://tinyfac.es/data/avatars/73788530-3C1E-4DB8-ABEA-148C8821389F-500w.jpeg"),
            new Pegawai(3, "Jodie Mantra", "Gianyar", "https://tinyfac.es/data/avatars/8BC1114A-5CD8-48DD-917E-EB9DAFA60DDB-500w.jpeg"),
            new Pegawai(4, "Arya Dharmaadi", "Denpasar", "https://tinyfac.es/data/avatars/E0B4CAB3-F491-4322-BEF2-208B46748D4A-500w.jpeg")
    ));

    public static Pegawai getPegawaiById(int idPegawai) {
        for (Pegawai itemPegawai : listPegawai) {
            if (itemPegawai.getId_pegawai() == idPegawai) {
                return itemPegawai;
            }
        }
        return null;
    }
}
