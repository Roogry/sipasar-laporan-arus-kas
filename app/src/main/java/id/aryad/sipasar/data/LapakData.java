package id.aryad.sipasar.data;

import java.util.ArrayList;
import java.util.List;

import id.aryad.sipasar.model.Lapak;

public class LapakData {
    public static ArrayList<Lapak> listLapak = new ArrayList<>(List.of(
            new Lapak(1, 1, "Klontong Mas", "Jodie Mantra", "Denpasar", "klontong_mas.jpg", "A1", 1, "2021-03-12 07:26:38", "2022-03-12 07:26:38", 1),
            new Lapak(2, 2, "Toko Sebelah", "Vidya Chandradev", "Tabanan", "toko_sebelah.jpg", "A2", 1, "2021-04-12 07:26:38", "2022-04-12 07:26:38", 1),
            new Lapak(3, 2, "Baju Kenangan", "Mang Wahyu", "Gianyar", "baju_kenangan.jpg", "A3", 1, "2021-05-12 07:26:38", "2022-05-12 07:26:38", 1)
    ));

    public static Lapak getLapakById(int idLapak) {
        for (id.aryad.sipasar.model.Lapak Lapak : listLapak) {
            if (Lapak.getId_lapak() == idLapak) {
                return Lapak;
            }
        }
        return null;
    }
}
