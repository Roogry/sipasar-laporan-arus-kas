package id.aryad.sipasar.data;

import java.util.ArrayList;
import java.util.List;

import id.aryad.sipasar.model.KategoriIuran;

public class KategoriIuranData {
    public static ArrayList<KategoriIuran> listKategoriIuran = new ArrayList<>(List.of(
            new KategoriIuran(1, "air", 50000, 1),
            new KategoriIuran(2, "listrik", 100000, 1),
            new KategoriIuran(3, "sampah", 20000, 1)
    ));

    public static KategoriIuran getKategoriIuranById(int idKategoriIuran) {
        for (KategoriIuran kategoriIruan : listKategoriIuran) {
            if (kategoriIruan.getId_kategori_iuran() == idKategoriIuran) {
                return kategoriIruan;
            }
        }
        return null;
    }
}
