package id.aryad.sipasar.ui.DetailArusKas;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager2.widget.ViewPager2;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import java.util.ArrayList;

import id.aryad.sipasar.R;
import id.aryad.sipasar.controller.PeriodeLaporanController;
import id.aryad.sipasar.helper.KeyValue;
import id.aryad.sipasar.helper.NumberHelper;
import id.aryad.sipasar.ui.DetailArusKas.pager.DetailArusKasPagerAdapter;

public class DetailArusKas extends AppCompatActivity {

    @StringRes
    private final int[] TAB_TITLES = new int[]{R.string.tab_title_1, R.string.tab_title_2, R.string.tab_title_3};
    BarChart barChart;
    BarData barData;
    BarDataSet barDataSet;
    ArrayList barEntriesArrayList;
    private TextView txtPeriod, txtNominalAwal, txtNominalAkhir;
    private ImageView mvBack;
    private TabLayout tabLayout;
    private ViewPager2 viewPager;
    private View bgDim;
    private LinearLayout bsChart, btnChart, btnDismissBS;
    private BottomSheetBehavior<LinearLayout> bsbChart;
    private PeriodeLaporanController periodeSekarang, periodeSebelumnya;
    private int idPeriode, barAxisMaximum;

    private void initLayout() {
        txtPeriod = findViewById(R.id.txtPeriod);
        txtNominalAwal = findViewById(R.id.txtNominalAwal);
        txtNominalAkhir = findViewById(R.id.txtNominalAkhir);
        mvBack = findViewById(R.id.ivBack);

        tabLayout = findViewById(R.id.tabs);
        viewPager = findViewById(R.id.view_pager);
        bgDim = findViewById(R.id.bgDim);

        barChart = findViewById(R.id.chartArusKas);
        bsChart = findViewById(R.id.bsChart);
        btnChart = findViewById(R.id.btnChart);
        btnDismissBS = findViewById(R.id.btnDismissBS);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_arus_kas);

        Intent intentExtra = getIntent();
        idPeriode = intentExtra.getIntExtra(KeyValue.EXT_PERIODE_ID, 0);

        this.initLayout();
        this.setupTabs();
        this.getPeriodeLaporan();
        this.setupBottomSheet();

        mvBack.setOnClickListener(v -> this.onBackPressed());
        btnChart.setOnClickListener(v -> bsbChart.setState(BottomSheetBehavior.STATE_EXPANDED));
        btnDismissBS.setOnClickListener(v -> bsbChart.setState(BottomSheetBehavior.STATE_HIDDEN));
    }

    private void setupBottomSheet() {
        bsbChart = BottomSheetBehavior.from(bsChart);
        bsbChart.setState(BottomSheetBehavior.STATE_HIDDEN);

        bsbChart.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED)
                    bgDim.setVisibility(View.GONE);
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                bgDim.setVisibility(View.VISIBLE);
                bgDim.setAlpha(slideOffset);
            }
        });
    }

    private void setupTabs() {
        DetailArusKasPagerAdapter pagerAdapter = new DetailArusKasPagerAdapter(this);
        viewPager.setAdapter(pagerAdapter);

        new TabLayoutMediator(tabLayout, viewPager,
                (tab, position) -> tab.setText(getResources().getString(TAB_TITLES[position]))
        ).attach();
    }

    private void getPeriodeLaporan() {
        periodeSekarang = new PeriodeLaporanController(idPeriode);

        if (periodeSekarang.getPeriode().getId_laporan_sebelumnya() != 0) {
            periodeSebelumnya = new PeriodeLaporanController(periodeSekarang.getPeriode().getId_laporan_sebelumnya());
            txtNominalAwal.setText(NumberHelper.currency(periodeSebelumnya.getPeriode().getUang_kas()));
        }

        txtPeriod.setText(periodeSekarang.getPeriode().getNama_periode());
        txtNominalAkhir.setText(NumberHelper.currency(periodeSekarang.getPeriode().getUang_kas()));

        if (periodeSekarang.getPeriode().getUang_kas() < 0) {
            txtNominalAkhir.setTextColor(getResources().getColor(R.color.red_orange));
        }

        setChartValue();
    }

    private void setupChart() {
        ArrayList<Integer> barColors = new ArrayList<>();
        barColors.add(ContextCompat.getColor(this, R.color.red_orange));
        barColors.add(ContextCompat.getColor(this, R.color.blue));

        barDataSet = new BarDataSet(barEntriesArrayList, "Pengeluaran & Pemasukan");
        barDataSet.setColors(barColors);
        barDataSet.setValueTextColor(Color.BLACK);
        barDataSet.setValueTextSize(10f);
        barDataSet.setDrawValues(true);

        barData = new BarData(barDataSet);

        barChart.setData(barData);
        barChart.getDescription().setEnabled(false);
        barChart.setPinchZoom(false);
        barChart.setDrawBorders(false);
        barChart.setDrawGridBackground(false);
        barChart.getAxisLeft().setAxisMinimum(0);
        barChart.getAxisLeft().setAxisMaximum(barAxisMaximum);
        barChart.getAxisRight().setEnabled(false);
        barChart.getXAxis().setEnabled(false);
        barChart.getLegend().setEnabled(false);
    }

    private void setChartValue() {
        barEntriesArrayList = new ArrayList<>();

        barEntriesArrayList.add(new BarEntry(1f, (periodeSekarang.getUangKeluar())));
        barEntriesArrayList.add(new BarEntry(2f, (periodeSekarang.getUangMasuk())));

        Log.d("chart", "pemasukan : " + periodeSekarang.getUangMasuk() + " dan pengeluaran : " + periodeSekarang.getUangKeluar());

        barAxisMaximum = Math.max(periodeSekarang.getUangKeluar(), periodeSekarang.getUangMasuk());
        barAxisMaximum += barAxisMaximum / 10;

        this.setupChart();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            if (bsbChart.getState() == BottomSheetBehavior.STATE_EXPANDED) {

                Rect outRect = new Rect();
                bsChart.getGlobalVisibleRect(outRect);

                if (!outRect.contains((int) ev.getRawX(), (int) ev.getRawY()))
                    bsbChart.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        }

        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void onPause() {
        super.onPause();
        bsbChart.setState(BottomSheetBehavior.STATE_HIDDEN);
    }
}