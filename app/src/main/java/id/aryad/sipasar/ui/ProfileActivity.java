package id.aryad.sipasar.ui;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;

import id.aryad.sipasar.R;
import id.aryad.sipasar.controller.AuthenticationController;
import id.aryad.sipasar.helper.KeyValue;
import id.aryad.sipasar.helper.SharedPref;

public class ProfileActivity extends AppCompatActivity {

    TextView tvNama, tvRole;
    ImageView ivPegawai, ivBack;
    LinearLayout btnLogout;

    private void initLayout() {
        tvNama = findViewById(R.id.tvNama);
        tvRole = findViewById(R.id.tvRole);
        ivPegawai = findViewById(R.id.ivPegawai);
        ivBack = findViewById(R.id.ivBack);
        btnLogout = findViewById(R.id.btnLogout);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        this.initLayout();

        SharedPref sharedPref = new SharedPref(this);
        tvNama.setText(sharedPref.getString(KeyValue.SP_NAME));
        tvRole.setText(sharedPref.getString(KeyValue.SP_ROLE));
        Glide.with(this)
                .load(sharedPref.getString(KeyValue.SP_PHOTO))
                .centerCrop()
                .placeholder(R.drawable.ex_ava)
                .into(ivPegawai);

        ivBack.setOnClickListener(v -> this.onBackPressed());
        btnLogout.setOnClickListener(v -> AuthenticationController.logout(this));
    }
}