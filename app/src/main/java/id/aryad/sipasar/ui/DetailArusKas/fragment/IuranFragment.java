package id.aryad.sipasar.ui.DetailArusKas.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import id.aryad.sipasar.R;
import id.aryad.sipasar.adapter.IuranAdapter;
import id.aryad.sipasar.data.PembayaranIuranData;
import id.aryad.sipasar.data.PeriodeLaporanData;
import id.aryad.sipasar.helper.KeyValue;
import id.aryad.sipasar.model.PembayaranIuran;
import id.aryad.sipasar.model.PeriodeLaporan;

public class IuranFragment extends Fragment {

    private RecyclerView rvIuran;
    private LinearLayout holderEmpty;

    private ArrayList<PembayaranIuran> listIuranPeriode;
    private PeriodeLaporan periode;

    private IuranAdapter arusKasAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_iuran, container, false);

        holderEmpty = view.findViewById(R.id.holderEmpty);
        rvIuran = view.findViewById(R.id.rvIuran);

        rvIuran.setHasFixedSize(true);
        rvIuran.setLayoutManager(new LinearLayoutManager(getContext()));

        Intent intentExtra = getActivity().getIntent();
        int idPeriode = intentExtra.getIntExtra(KeyValue.EXT_PERIODE_ID, 0);

        periode = PeriodeLaporanData.getPeriodeById(idPeriode);

        listIuranPeriode = new ArrayList<>(PembayaranIuranData.getByPeriode(
                PembayaranIuranData.listPembayaranIuran,
                periode.getTanggal_awal_periode(),
                periode.getTanggal_akhir_periode()
        ));

        if (!listIuranPeriode.isEmpty()) {
            holderEmpty.setVisibility(View.GONE);
        }

        arusKasAdapter = new IuranAdapter(listIuranPeriode);
        rvIuran.setAdapter(arusKasAdapter);

        return view;
    }
}