package id.aryad.sipasar.ui;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import id.aryad.sipasar.R;
import id.aryad.sipasar.controller.AuthenticationController;
import id.aryad.sipasar.helper.SharedPref;
import id.aryad.sipasar.helper.StatusCode;

public class LoginActivity extends AppCompatActivity {

    private EditText edtUsername, edtPassword;
    private Button btnLogin;

    private void initLayout() {
        edtUsername = findViewById(R.id.edtUsername);
        edtPassword = findViewById(R.id.edtPassword);
        btnLogin = findViewById(R.id.btnLogin);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.initLayout();
        SharedPref sharedPref = new SharedPref(this);

        // Check if Already Logged In
        if (sharedPref.isLoggined()) {
            toArusKasActivity();
        }

        btnLogin.setOnClickListener(v -> {
            String username = edtUsername.getText().toString();
            String password = edtPassword.getText().toString();

            int resultLogin = AuthenticationController.login(this, username, password);

            if (resultLogin == StatusCode.SUCCESS) {
                toArusKasActivity();
            } else if (resultLogin == StatusCode.BAD_REQUEST) {
                Toast.makeText(this, "Inputan tidak valid", Toast.LENGTH_SHORT).show();
            } else if (resultLogin == StatusCode.NOT_FOUND) {
                Toast.makeText(this, "Username atau Password salah", Toast.LENGTH_SHORT).show();
            } else if (resultLogin == StatusCode.FORBIDDEN) {
                Toast.makeText(this, "Anda tidak berwenang", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Error, Mohon coba lagi lain waktu", Toast.LENGTH_SHORT).show();
            }

        });
    }

    private void toArusKasActivity() {
        Intent toArusKas = new Intent(LoginActivity.this, LaporanArusKasActivity.class);
        toArusKas.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(toArusKas);
        finish();
    }

}