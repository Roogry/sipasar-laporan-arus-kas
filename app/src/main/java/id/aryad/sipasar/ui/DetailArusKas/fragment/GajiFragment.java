package id.aryad.sipasar.ui.DetailArusKas.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import id.aryad.sipasar.R;
import id.aryad.sipasar.adapter.GajiAdapter;
import id.aryad.sipasar.data.PembayaranGajiData;
import id.aryad.sipasar.data.PeriodeLaporanData;
import id.aryad.sipasar.helper.KeyValue;
import id.aryad.sipasar.model.PembayaranGaji;
import id.aryad.sipasar.model.PeriodeLaporan;

public class GajiFragment extends Fragment {

    private RecyclerView rvGaji;
    private LinearLayout holderEmpty;

    private ArrayList<PembayaranGaji> listGajiPeriode;
    private PeriodeLaporan periode;

    private GajiAdapter gajiAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_gaji, container, false);

        holderEmpty = view.findViewById(R.id.holderEmpty);
        rvGaji = view.findViewById(R.id.rvGaji);

        rvGaji.setHasFixedSize(true);
        rvGaji.setLayoutManager(new LinearLayoutManager(getContext()));

        Intent intentExtra = getActivity().getIntent();
        int idPeriode = intentExtra.getIntExtra(KeyValue.EXT_PERIODE_ID, 0);

        periode = PeriodeLaporanData.getPeriodeById(idPeriode);

        listGajiPeriode = new ArrayList<>(PembayaranGajiData.getByPeriode(
                PembayaranGajiData.listPembayaranGaji,
                periode.getTanggal_awal_periode(),
                periode.getTanggal_akhir_periode()
        ));

        if (!listGajiPeriode.isEmpty()) {
            holderEmpty.setVisibility(View.GONE);
        }

        gajiAdapter = new GajiAdapter(getContext(), listGajiPeriode);
        rvGaji.setAdapter(gajiAdapter);
        return view;
    }
}