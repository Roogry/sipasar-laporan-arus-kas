package id.aryad.sipasar.ui.DetailArusKas.pager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import id.aryad.sipasar.ui.DetailArusKas.fragment.GajiFragment;
import id.aryad.sipasar.ui.DetailArusKas.fragment.IuranFragment;
import id.aryad.sipasar.ui.DetailArusKas.fragment.KontrakFragment;

public class DetailArusKasPagerAdapter extends FragmentStateAdapter {
    public DetailArusKasPagerAdapter(@NonNull AppCompatActivity activity) {
        super(activity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new IuranFragment();
                break;
            case 1:
                fragment = new KontrakFragment();
                break;
            case 2:
                fragment = new GajiFragment();
                break;
        }
        return fragment;
    }

    @Override
    public int getItemCount() {
        return 3;
    }
}
