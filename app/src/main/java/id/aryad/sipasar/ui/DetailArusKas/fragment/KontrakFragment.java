package id.aryad.sipasar.ui.DetailArusKas.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import id.aryad.sipasar.R;
import id.aryad.sipasar.adapter.KontrakAdapter;
import id.aryad.sipasar.data.PembayaranKontrakData;
import id.aryad.sipasar.data.PeriodeLaporanData;
import id.aryad.sipasar.helper.KeyValue;
import id.aryad.sipasar.model.PembayaranKontrak;
import id.aryad.sipasar.model.PeriodeLaporan;

public class KontrakFragment extends Fragment {

    private RecyclerView rvKontrak;
    private LinearLayout holderEmpty;

    private ArrayList<PembayaranKontrak> listKontrakPeriode;
    private PeriodeLaporan periode;

    private KontrakAdapter kontrakAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_kontrak, container, false);

        holderEmpty = view.findViewById(R.id.holderEmpty);
        rvKontrak = view.findViewById(R.id.rvKontrak);

        rvKontrak.setHasFixedSize(true);
        rvKontrak.setLayoutManager(new LinearLayoutManager(getContext()));

        Intent intentExtra = getActivity().getIntent();
        int idPeriode = intentExtra.getIntExtra(KeyValue.EXT_PERIODE_ID, 0);

        periode = PeriodeLaporanData.getPeriodeById(idPeriode);

        listKontrakPeriode = new ArrayList<>(PembayaranKontrakData.getByPeriode(
                PembayaranKontrakData.listPembayaranKontrak,
                periode.getTanggal_awal_periode(),
                periode.getTanggal_akhir_periode()
        ));

        if (!listKontrakPeriode.isEmpty()) {
            holderEmpty.setVisibility(View.GONE);
        }

        kontrakAdapter = new KontrakAdapter(listKontrakPeriode);
        rvKontrak.setAdapter(kontrakAdapter);
        return view;
    }
}