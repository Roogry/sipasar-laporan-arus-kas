package id.aryad.sipasar.ui;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import id.aryad.sipasar.R;
import id.aryad.sipasar.adapter.PeriodeAdapter;
import id.aryad.sipasar.data.PeriodeLaporanData;
import id.aryad.sipasar.model.PeriodeLaporan;

public class LaporanArusKasActivity extends AppCompatActivity {

    private ImageView mvProfile;
    private RecyclerView rvPeriode;

    private PeriodeAdapter periodeAdapter;

    private final ArrayList<PeriodeLaporan> listPeriode = new ArrayList<>(PeriodeLaporanData.listPeriodeLaporan);

    private void initLayout() {
        mvProfile = findViewById(R.id.mvProfile);

        rvPeriode = findViewById(R.id.rvPeriode);
        rvPeriode.setLayoutManager(new LinearLayoutManager(this));
        rvPeriode.setHasFixedSize(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laporan_arus_kas);
        this.initLayout();

        mvProfile.setOnClickListener(v -> {
            Intent toProfile = new Intent(LaporanArusKasActivity.this, ProfileActivity.class);
            startActivity(toProfile);
        });

        periodeAdapter = new PeriodeAdapter(this, listPeriode);
        rvPeriode.setAdapter(periodeAdapter);
    }
}