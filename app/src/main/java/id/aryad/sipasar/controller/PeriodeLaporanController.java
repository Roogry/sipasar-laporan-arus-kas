package id.aryad.sipasar.controller;

import java.util.ArrayList;

import id.aryad.sipasar.data.PembayaranGajiData;
import id.aryad.sipasar.data.PembayaranIuranData;
import id.aryad.sipasar.data.PembayaranKontrakData;
import id.aryad.sipasar.data.PeriodeLaporanData;
import id.aryad.sipasar.model.PembayaranGaji;
import id.aryad.sipasar.model.PembayaranIuran;
import id.aryad.sipasar.model.PembayaranKontrak;
import id.aryad.sipasar.model.PeriodeLaporan;

public class PeriodeLaporanController {
    private final PeriodeLaporan periode;
    private int periodePos;
    private int pemasukan;
    private int pengeluaran;

    public PeriodeLaporanController(int idPeriode) {
        pemasukan = 0;
        pengeluaran = 0;
        periode = PeriodeLaporanData.getPeriodeById(idPeriode);
        if (periode != null) {
            periodePos = PeriodeLaporanData.getPeriodePositionById(idPeriode);
            updateUangKas();
        }
    }

    public PeriodeLaporan getPeriode() {
        return periode;
    }

    public int getUangMasuk() {
        return pemasukan;
    }

    public int getUangKeluar() {
        return pengeluaran;
    }

    private void updateUangKas() {
        ArrayList<PembayaranIuran> listIuran;
        ArrayList<PembayaranKontrak> listKontrak;
        ArrayList<PembayaranGaji> listGaji;

        listIuran = new ArrayList<>(PembayaranIuranData.getByPeriode(
                PembayaranIuranData.listPembayaranIuran,
                periode.getTanggal_awal_periode(),
                periode.getTanggal_akhir_periode()
        ));

        listKontrak = new ArrayList<>(PembayaranKontrakData.getByPeriode(
                PembayaranKontrakData.listPembayaranKontrak,
                periode.getTanggal_awal_periode(),
                periode.getTanggal_akhir_periode()
        ));

        listGaji = new ArrayList<>(PembayaranGajiData.getByPeriode(
                PembayaranGajiData.listPembayaranGaji,
                periode.getTanggal_awal_periode(),
                periode.getTanggal_akhir_periode()
        ));

        // reset uang kas sebelum di hitung ulang
        setArusKas(0);

        // menambahkan uang kas sebelumnya
        PeriodeLaporan periodeSebelumnya = PeriodeLaporanData.getPeriodeById(periode.getId_laporan_sebelumnya());
        if (periodeSebelumnya != null) {
            setArusKas(periodeSebelumnya.getUang_kas());
        }

        // mnambahkan uang kas dari pembayaran iuran
        for (PembayaranIuran iuran : listIuran) {
            increaseKas(iuran.getNilai());
        }

        // mnambahkan uang kas dari pembayaran kontrak
        for (PembayaranKontrak kontrak : listKontrak) {
            increaseKas(kontrak.getNilai());
        }

        // mengurangi uang kas dari pembayaran gaji
        for (PembayaranGaji gaji : listGaji) {
            decreaseKas(gaji.getNilai_gaji());
        }
    }

    private void setArusKas(int nominal) {
        PeriodeLaporanData.listPeriodeLaporan.get(periodePos).setUang_kas(nominal);
    }

    private void increaseKas(int nominal) {
        pemasukan += nominal;
        PeriodeLaporanData.listPeriodeLaporan.get(periodePos).increaseKas(nominal);
    }

    private void decreaseKas(int nominal) {
        pengeluaran += nominal;
        PeriodeLaporanData.listPeriodeLaporan.get(periodePos).decreaseKas(nominal);
    }
}
