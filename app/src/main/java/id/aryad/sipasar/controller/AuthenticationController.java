package id.aryad.sipasar.controller;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import java.util.ArrayList;

import id.aryad.sipasar.data.AdminData;
import id.aryad.sipasar.data.PegawaiData;
import id.aryad.sipasar.helper.SharedPref;
import id.aryad.sipasar.helper.StatusCode;
import id.aryad.sipasar.model.Admin;
import id.aryad.sipasar.model.AdminRole;
import id.aryad.sipasar.model.Pegawai;
import id.aryad.sipasar.ui.LoginActivity;

public class AuthenticationController {

    public static void logout(Context context) {
        SharedPref sharedPref = new SharedPref(context);
        sharedPref.clearSession();

        Intent toLogin = new Intent(context, LoginActivity.class);
        toLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(toLogin);

        Toast.makeText(context, "Silahkan login kembali", Toast.LENGTH_SHORT).show();
    }

    public static int login(Context context, String username, String password) {
        SharedPref sharedPref = new SharedPref(context);

        ArrayList<Admin> listAdmin = new ArrayList<>(AdminData.listAdmin);

        if (!username.trim().isEmpty() && !password.trim().isEmpty()) {

            for (Admin admin : listAdmin) {
                if (admin.getUsername().equals(username) && admin.getPassword().equals(password)) {
                    if (admin.getStatus() == 1 && admin.getRole().equals(AdminRole.MANAGER)) {
                        // get data pegawai of the admin
                        Pegawai pegawai = PegawaiData.getPegawaiById(admin.getId_pegawai());

                        // set session
                        sharedPref.setSessionLogin(admin.getId_admin(), pegawai.getNama_pegawai(), pegawai.getFoto(), admin.getRole());
                        return StatusCode.SUCCESS;
                    } else {
                        return StatusCode.FORBIDDEN;
                    }
                }
            }

            return StatusCode.NOT_FOUND;
        } else {
            return StatusCode.BAD_REQUEST;
        }
    }
}
