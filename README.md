**Android java based system**

SiPasar is a market management system in Indonesia. This system has a variety of market management features such as payment of dues, payment of contracts, payment of employee salaries, and others. The following repository contains one of its features, namely viewing the Cash Flow Statement.

The technology in it:
* Data from static Array List
* Update static Array List Data
* Looping with For Each List Object
* Login Validation with Status Code
* Session with SharedPreferences
* Custom Rounded Background
* Recycler View
* Ripple Effect
* Bottom Sheet Dialog

Third Party Library:
* BarChart from MPAndroidChart Library
* Glide library
